function addTokens(input, tokens){
    	if(typeof input != "string"){
    		throw "Invalid input";
    	}
    	if(input.length < 6){
    		throw "Input should have at least 6 characters";
    	}
    	if(typeof tokens[0].tokenName != "string"){
    		throw "Invalid array format";
    	}
    		var m = 0;
			var x;
			var i=0;
			var res = input.split(/[ ;,]+/);
			for(x=0; x<res.length; x++){
			  	if(res[x] === "..."){
			   		res[x] = "";
			   		res[x] += " ${" + tokens[i].tokenName + "}";
			   		i++;
			   		m = 1;
			   	}
			}
			var output = "";
			if(m === 1)
			{
			  	for(x=0; x<res.length; x++){
			   		output += res[x];
			   	}
			   	return output;
			}
			if(m === 0){
			   	return input;
			}
}

const app = {
    addTokens: addTokens
}

module.exports = app;